from rest_framework import viewsets
from scraper.models import Store, Product
from scraper.serializer import ProductSerializer, StoreSerializer
from django.views.generic import TemplateView
from scraper.search_util import search_product

class StoreViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    filter_fields = ['location', 'name']


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_fields = ['name', 'price', 'store', 'strain']

class SearchProductView(TemplateView):
    template_name = "search.html"

    def get_context_data(self, **kwargs):
        context = super(SearchProductView, self).get_context_data(**kwargs)
        context['products'] = [i for i in search_product(self.args[0])]
        return context