from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from scraper.models import Product, Store
from scraper.search import ProductIndex
from elasticsearch_dsl import Search
from dealscraper.settings import ES_URL

def bulk_indexing():
    ProductIndex.init()
    es = Elasticsearch(hosts=[ES_URL], request_timeout=100)
    bulk(client=es, actions=(b.indexing() for b in Product.objects.all().iterator()))


def search_base(instance: object, index: object, text: object) -> object:
    s = Search(index=index).query('match', text=text)
    response = s.execute()
    for r in response:
        yield instance.objects.get(pk=r.meta.id)

def search_product(text):
    return search_base(instance=Product, index="product", text=text)


def search_store(text):
    return search_base(instance=Store, index="store", text=text)
