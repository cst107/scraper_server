from django.db import models
from django.template.defaultfilters import slugify
from scraper.search import ProductIndex
from django.db.models.signals import post_save
from django.dispatch import receiver


class Store(models.Model):
    """
        Represents a Store
    """
    location = models.CharField(max_length=30, default="Anywhere")
    url = models.URLField(default="Not Available")  # The store url
    name = models.CharField(max_length=30)  # Name of the store
    slug = models.SlugField(default='')

    class Meta:
        unique_together = (("location", "url"),)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Store, self).save(*args, **kwargs)


class Product(models.Model):
    """
        Represents a single product. A product is also associated with a corresponding store
    """
    name = models.CharField(max_length=30)
    price = models.FloatField()  # The base price of the product (e.g. $6/gram)
    store = models.ForeignKey(Store, on_delete=models.CASCADE, related_name='products')
    url = models.URLField(default=None)  # The actual URL of the product
    strain = models.CharField(max_length=30)
    slug = models.SlugField(default='')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Product, self).save(*args, **kwargs)


    def indexing(self):
        obj = ProductIndex(
            meta={
                'id': self.id,
            },
            name=self.name,
            strain=self.strain,
            text="%s %s %s" % (self.name, self.strain, self.store.name)
        )
        print(obj)
        obj.save()
        return obj.to_dict(include_meta=True)


def index_post(sender, instance, **kwargs):
    instance.indexing()

post_save.connect(index_post, sender=Product)
