import requests
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand
from scraper.models import Store, Product
import asyncio
import async_timeout
import aiohttp


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.scrape_cannibismo()
        self.scrape_ganjaexpress()

    def scrape_ganjaexpress(self):
        """
            Prints the product listing of marijuana flowers found in:
            https://ganjaexpress.ca/product-category/buy-marijuana-buds-online
        """
        url = 'https://ganjaexpress.ca/product-category/buy-marijuana-buds-online/'
        store, created = Store.objects.get_or_create(name="Ganja Express", url=url)
        store.save()

        r = requests.get(url, verify=False)
        soup = BeautifulSoup(r.text, 'html.parser')
        links = soup.find_all("div", class_="desc")
        loop = asyncio.get_event_loop()

        async def sub_tree(l):
            await asyncio.sleep(0)
            desc = l.find(class_="pro_price_extra_info")
            if desc:
                a = l.find_all('a')[0]
                (link, label) = (a['href'], str(a.string))
                price = float(desc.string.strip().split(" ")[0][1:])
                conn = aiohttp.TCPConnector(verify_ssl=False)
                async with aiohttp.ClientSession(loop=loop, connector=conn) as session:
                    detail = await fetch(session, link)
                    soup_in = BeautifulSoup(detail, 'html.parser')  # Now we scrape the strain
                    strain = soup_in.select(".tagged_as a")[0].string.split(" ")[0]
                    product, _ = Product.objects.get_or_create(strain=strain, price=price, url=link, name=label,
                                                               store=store)
                    product.save()

        async def fetch(session, url):
            with async_timeout.timeout(10):
                async with session.get(url) as response:
                    return await response.text()

        futures = [loop.create_task(sub_tree(l)) for l in links]
        loop.run_until_complete(asyncio.wait(futures))
        loop.close()



    def scrape_cannibismo(self):
        """
            Prints the product listing of marijuana flowers found in:
            https://cannabismo.ca/flowers/
        """
        url = 'https://cannabismo.ca/flowers/'
        store, created = Store.objects.get_or_create(name="Cannabismo")
        if created:
            store.url = url
            store.name = "Cannabismo"
            store.save()

        r = requests.get(url, verify=True)
        soup = BeautifulSoup(r.text, 'html.parser')
        links = soup.find_all("div", class_="product-details")
        for l in links:
            title = l.find(class_="product-title")
            strain_el = l.find(class_="products-page-cats")

            strain = str(strain_el.contents[len(strain_el.contents) - 2].string)
            price = float(l.select(".woocommerce-Price-amount")[0].contents[1])

            product, _ = Product.objects.get_or_create(strain=strain, price=price, url=title.a['href'],
                                                       name=str(title.a.string), store=store)
            product.save()
