from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Integer
from dealscraper import settings

connections.create_connection(hosts=[settings.ES_URL], timeout=1000)


class ProductIndex(DocType):
    class Meta:
        index = 'product'
    name = Text()
    strain = Text()
    text = Text()

