from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from scraper.views import ProductViewSet, StoreViewSet, SearchProductView
from rest_framework.documentation import include_docs_urls


API_TITLE = 'Scraper Docs'
API_DESCRIPTION = 'This is where you find in-depth information for the endpoints'
router = routers.DefaultRouter()
router.register(r'store', StoreViewSet)
router.register(r'product', ProductViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
    url(r'^search-prod/([\w-]+)/$', view=SearchProductView.as_view())
]
