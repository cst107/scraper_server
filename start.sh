# if you dont have elasticsearch server running yet, uncomment line below
# docker run -p 9200:9200 -e "http.host=0.0.0.0" -e "transport.host=127.0.0.1" -t docker.elastic.co/elasticsearch/elasticsearch:5.1.2


if ! grep -q "export ES_URL=http://elastic:changeme@0.0.0.0:9200" "$HOME/.bashrc"; then
    echo export ES_URL="http://elastic:changeme@0.0.0.0:9200" >> ~/.bashrc
else
    echo "ES_URL environment variable already exist"
fi

