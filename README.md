# Scraper
The backend-end server for pricematching marijuana flowers found on supported e-commerce websites.

## Requirements
1. Python 3
2. Virtualenvironment

## Installation
1. Create a python virtual environment and activate it
2. Install the dependencies by typing: 
```
	pip install -r requirements.txt
```
3. Create migrations & migrate them 
4. Run the server

## Supported e-commerce websites
Right now, we only have website supported. 

1. Cannabismo

## Filtering products/store via URLs
TODO: Add some samples here


## API Guide
This server provide RESTful service for querying information

## Endpoints
**/store** - gives you a list of stores 

**/products** - gives you a list of products 


## Querying
The server also offers query and filter features. 

### Examples: 
Query a store based on its name: 

```
	/store/?name=Cannabismo
```

This will query the store that has a name of **Cannabismo**

## Special Queries
### Ordering Filters
You can also sort results based its field

For example, if you want the results from the ```/product``` endpoint to be se sorted in ascending/descending manner. 

**Ascending**
```
	/product/?ordering=price
```

**Descending**
```
	/product/?ordering=-price
```  


## Database
Since this project is fairly small, I decided to keep it simple. 

Database Used: SQLlite

To update the data, execute the command below: 
```
	python manage.py refresh_content
```


